# ApexStreams

ApexStreams is a library intended to emulate the syntax around the Java Streams implementation.  This includes lazy iteration, and an approximation of the Functional interfaces.  In order to work around limitations with Apex handling of
Generics, design decisions were made to balance the need for code reuse and the unpleasantness of non-type safe casting.  
In addition, care must be taken to avoid type errors as the compiler isn't going to catch chained functions not properly typed. 

Lastly, the performance of Lazy Iteration in **most** cases is relatively comparable to raw for loop syntax.  However, when debugging Apex the cost of indirection is quite high as logging for every method entry/exit is logged.  This can lead to a performance loss as much as a factor of ten when logging is turned on in it's full glory.  In most cases, this is not an issue but for records in the order of thousands I've definitely seen some CPU timeouts. 

## Dependencies

Raw Apex Baby! Deploy and profit 

### Functions and Predicates

The building blocks to the streams implementation are **Functions** and **Predicates** and **BiCollectorFunction** (Oh my)

Functions implement *Object apply(Object arg)* and Predicates implement *Boolean apply(Object arg)*
In Java, predicates are really Function<Boolean,?> but I'll kick the dead horse and mention that because of non-support 
for true generics, the design decision was made to include these as two seperate unrelated interfaces to eliminate the need for (Boolean)predicate.apply(). 

*BiCollector Functions* are intended to specifically implement aggregator functions for collection. They do as the name suggests, and accept two arguments *Object apply(Object one, Object two)*.  The first argument is intended as the passthrough aggregation result(either null for the first execution, or the result of prior executions). As a result, implementers of this function should have a null check as their base condition along with the aggregate result they are supplying. 

# Examples
  There are some convenience utility classes for commonly used functions that will be added as more become available, along with the ability to Join Functions.  The interfaces themselves are relatively easy to implement, but here's some code patterns using those custom packages. 

  **Return true when Account name ='Foo'**
  `Predicates.piping(Functions.getFieldValue('Name'),Predicates.eq('Foo'))`
 
  **Return true when the Account creation date < today**
  `Predicate pred = Predicates.piping(Functions.getFieldValue('DateCreated'), Comparables.toComparable(1));`
    *Note - Primitives don't adapt well to the Interface Comparable, so Comparables.toComparable is a utility to allow for this*

## Lazy Iteration

The Stream interface makes use of the predicate, functional, and Bicollecors in order to provide the concise syntax familiar from java streams along with the lazy implementation. The currently implemented pieces are filter(), map() ,
collect(), and forEach().  Here are some examples of use cases and the syntax

**For each accountRecord, if the email is blank and the phone is blank**
```
{
Predicate emailAndPhoneAreBlank = Predicates.forFieldValue('Email',Predicates.eq(null))
  .andAlso(Predicates.forFieldValue('Phone',Predicates.eq(null)));

Stream.of([Select name, email from Account])
.filter(emailAndPhoneAreBlank)
.foreach(Functions.addSobjectError('Email cannot be null!')) 
} 
```

## Subscriptions

The Streams invocation also enforces a Subscriber /publisher relationship. This is useful if you wish to control the consumption of a Stream based on runtime conditions. 

For example, here's a possible "best effort" implementation based on apex limits(code limited for brevity)

``` 
{
       public class MockSubscriber implements Subscriber {
        public void onComplete() {        }

        public void onNext(Object o) {
           if(Limits.getCPUTime() > THRESHHOLD) {
             this.subscription.cancel();
           } else {
             doAThing(o);
           }
        }

        public void onSubscribe(Subscription subscription) {
            subscription.request(10000);
        }
  }

  Stream stream = Streams.streamOf(new List<Object>(stuff));
  stream.subscribe(new MockSubscriber());//Nothing happens until you call this!!
  if(stream.hasNext()) {//Canceled due to approaching thresholdLimits
    System.enqueueJob(new QueueableThatDoesThingAndTakesStream(stream)); //Delegate the rest until later!
  }
}
```





