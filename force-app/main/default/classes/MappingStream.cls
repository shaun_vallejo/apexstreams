/*
 * Created on Tue Dec 08 2020
 *
 * Copyright (c) 2020 Sparta Systems
 * Author: Shaun Vallejo
 */

global inherited sharing class MappingStream extends Stream {

    private final Function mappingFunction;

    global MappingStream(Iterator<Object> iterator,Function mappingFunction) {
        super(iterator);
        this.mappingFunction = mappingFunction;
    }

    public override Object next() {
        return mappingFunction.apply(super.next());
    }


}
