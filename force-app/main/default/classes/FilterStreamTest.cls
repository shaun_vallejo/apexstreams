/*
 * Created on Tue Dec 08 2020
 *
 * Copyright (c) 2020 Sparta Systems
 * Author: Shaun Vallejo
 */

@IsTest
public with sharing class FilterStreamTest {

    public class MappingFilterFunction implements Predicate {
        public Boolean apply(Object arg) {
            Account acc = (Account)arg;
            return acc.Name != null && acc.name.equals('Foo');
        }
    }

    @IsTest
    public static void filterShouldComeUpWithTheRightNumberOfValues() {
        List<Object> accounts = new List<Account> {
            new Account(Name='Foo'),
            new Account(Name='Bar'),
            new Account(Name='Foo')
        };

        Integer count = 0;
        FilteringStream stream = new FilteringStream(accounts.iterator(),new MappingFilterFunction());
        while(stream.hasNext()) {
            stream.next();
            count ++;
        }
        System.assertEquals(2, count,'Loop did not run the correct number of times');
    }

    @IsTest
    public static void filterShouldNotReturnNextIfNoValuesApply() {
        List<Object> accounts = new List<Account> {
            new Account(Name='Foos'),
            new Account(Name='Bar'),
            new Account(Name='Foos')
        };

        Integer count = 0;
        FilteringStream stream = new FilteringStream(accounts.iterator(),new MappingFilterFunction());
        while(stream.hasNext()) {
            stream.next();
            count ++;
        }
        System.assertEquals(0, count,'Loop did not run the correct number of times');
    }

}
