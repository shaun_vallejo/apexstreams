/*
 * Created on Tue Dec 08 2020
 *
 * Copyright (c) 2020 Sparta Systems
 * Author: Shaun Vallejo
 */

public with sharing class Predicates {

    public class JoiningPredicate implements Predicate {
        public Predicate wrapper;

        public JoiningPredicate(Predicate decorated) {
            this.wrapper = decorated;
        }

        public Boolean apply(Object o) {
            return wrapper.apply(o);
        }

        public JoiningPredicate andAlso(Predicate arg) {
            this.wrapper = new ConjunctivePredicate(wrapper,arg);
            return this;
        }

        public JoiningPredicate orIs(Predicate arg) {
            this.wrapper = new DisjunctivePredicate(wrapper,arg);
            return this;
        }

    }

    public class ConjunctivePredicate implements Predicate {
        private final Predicate one;
        private final Predicate two;

        public ConjunctivePredicate(Predicate one, Predicate two) {
            this.one =one;
            this.two = two;
        }

        public Boolean apply(Object arg) {
            return one.apply(arg) && two.apply(arg);
        }
    }

    public class DisjunctivePredicate implements Predicate {
        private final Predicate one;
        private final Predicate two;

        public DisjunctivePredicate(Predicate one, Predicate two) {
            this.one = one;
            this.two = two;
        }

        public boolean apply(Object arg) {
            return one.apply(arg) || two.apply(arg);
        }
    }

    public class  Untrue implements Predicate {
        private final Predicate base; 
        
        public Untrue(Predicate base) {
            this.base = base;
        }

        public Boolean apply(Object arg) {
            return !base.apply(arg);
        }
    }

    public class Eq implements Predicate {
        public final Object base;

        public Eq(Object base)  {
            this.base = base;
        }
        public Boolean apply(Object o) {
            return (base == null && o == null) ||
            base.equals(o);
        }
    }

    public class Gt implements Predicate {
        public final Comparable base;
        public Gt(Comparable base) {
            this.base = base;
        }

        public Boolean apply(Object comparable) {
            return base.compareTo(comparable) < 0;
        }
    }

    public class Lt implements Predicate {
        private final Comparable base;

        private Lt(Comparable base) {
            this.base = base;
        }

        public Boolean apply(Object to) {
            return this.base.compareTo(to) > 0;
        }
    }

    public class MemberOf implements Predicate {
        private final Set<Object> items;
        public MemberOf(Set<Object> items) {
            this.items = items;
        }
        public Boolean apply(Object o) {
            return this.items.contains(o);
        }
    }

    public class PipingPredicate implements Predicate {
        private final Function pipe;
        private final Predicate predicate;
        public PipingPredicate(Function pipe, Predicate predicate) {
            this.pipe = pipe;
            this.predicate = predicate;
        }

        public Boolean apply(Object arg) {
            return predicate.apply(pipe.apply(arg));
        }
    }

    public static JoiningPredicate memberOf(Set<Object> items) {
        return new JoiningPredicate(new MemberOf(items));
    }

    public static JoiningPredicate eq(Object base) {
        return new JoiningPredicate(new Eq(base));
    } 

    public static JoiningPredicate gt(Comparable base) {
        return new JoiningPredicate(new Gt(base));
    }

    public static JoiningPredicate lt(Comparable base) {
        return new JoiningPredicate(new Lt(base));
    }

    public static Predicate isnt(Predicate predicate) {
        return new Untrue(predicate);
    }

    public static Predicate piping(Function pipe, Predicate predicate) {
        return new JoiningPredicate(new PipingPredicate(pipe,predicate));
    }

    public static Predicate forFieldValue(String fieldName, Predicate predicate) {
        return new JoiningPredicate(new PipingPredicate(Functions.fieldValueOf(fieldName),predicate));
    }

}
