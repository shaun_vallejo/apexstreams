/*
 * Created on Tue Dec 08 2020
 *
 * Copyright (c) 2020 Sparta Systems
 * Author: Shaun Vallejo
 */


public with sharing class Assert {
    private Assert() {}

    public static void notNull(Object o, String message) {
        if(o == null) {
            throw new InvalidArgumentException(message);
        }
    }

    public static void isNot(Predicate filter, String message,Object arg) {
        if(filter.apply(arg)) {
            throw new InvalidArgumentException(message);
        }
    }

    public static void isType(Type type, Object o , String message) {
        if(o instanceOf Type) {
            return;
        }
        throw new InvalidArgumentException(message);
    }
}
