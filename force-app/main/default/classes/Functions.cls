/*
 * Created on Tue Dec 08 2020
 *
 * Copyright (c) 2020 Sparta Systems
 * Author: Shaun Vallejo
 */

public with sharing class Functions {

   private  Functions() {}

   private class Identity implements Function {

      public Object apply(Object arg) {
         return arg;
      }
   }

   private class FieldValueOf implements Function {
      private final String fieldName;
      public FieldValueOf(String fieldName) {
         this.fieldName = fieldName;
      }

      public Object apply(Object arg) {
         SObject obj = (SObject)arg;
         return obj.get(this.fieldName);
      }
   }

   public class ChainingFunction implements Function {
      private final Function first;
      private final Function second;

      public ChainingFunction(Function first, Function second) {
         this.first = first;
         this.second = second;
      }

      public Object apply(Object arg) {
         return second.apply(first.apply(arg));
      }
   }

   public class SwitchingFunction implements Function {
      private final Function first;
      private final Function orThis;
      private final Predicate routing;
      
      public SwitchingFunction(Function first, Function orThis, Predicate routing) {
         this.first = first;
         this.orThis = orThis;
         this.routing = routing;
      }

      public Object apply(Object arg) {
         if(routing.apply(arg)) {
           return orThis.apply(arg);
         } else {
            return first.apply(arg);
         }
      }
   }

   public class AddExceptionToSObject implements Function {
      private final String errorMessage;

      public AddExceptionToSObject(String errorMessage) {
         this.errorMessage = errorMessage;
      }

      public Object apply(Object o) {
         SObject toError = (SObject)o;
         toError.addError(this.errorMessage);
         return null;
      }
   }

   public virtual class JoiningFunction implements Function {
      private final Function decorated;
      public JoiningFunction(Function decorated) {
         this.decorated = decorated;
      }

      public Object apply(Object arg) {
         return this.decorated.apply(arg);
      }

      public JoiningFunction andThen(Function function) {
         return new JoiningFunction(new ChainingFunction(this.decorated,function));
      }

      public JoiningFunction orApplyWhen(Function function,Predicate condition) {
         return new JoiningFunction(new SwitchingFunction(decorated,function,condition));
      }
   }

   public class GroupingBy implements BiCollectorFunction {
      private final Function routingFunction;

      public GroupingBy(Function routingFunction ) {
         this.routingFunction = routingFunction;
      }
      public Object apply(Object base, Object arg) {
         Map<Object,List<Object>> collectionMap;

         if(base != null) {
            collectionMap = (Map<Object,List<Object>>)base;
         } else {
            collectionMap = new Map<Object,List<Object>>();
         }

         Object key = this.routingFunction.apply(arg);
         if(collectionMap.containsKey(key)) {
            collectionMap.get(key).add(arg);
         } else {
            collectionMap.put(key,new List<Object>{arg});
         }

         return collectionMap;
      }
   }

   public static JoiningFunction identity() {
      return new JoiningFunction(new Identity());
   }

   public static JoiningFunction fieldValueOf(String fieldName) {
      return new JoiningFunction(new fieldValueOf(fieldName));
   }

   public static BiCollectorFunction grouping(Function routingFunction) {
      return new GroupingBy(routingFunction);
   }

   public static JoiningFunction addSObjectError(String message) {
      return new JoiningFunction(new AddExceptionToSObject(message));
   }

}
