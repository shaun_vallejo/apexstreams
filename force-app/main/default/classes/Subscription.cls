global interface Subscription {
    void cancel();

    void request(Long numberOfItems);
}
