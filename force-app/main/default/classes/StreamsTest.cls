/*
 * Created on Tue Dec 08 2020
 *
 * Copyright (c) 2020 Sparta Systems
 * Author: Shaun Vallejo
 */

@IsTest
public with sharing class StreamsTest {

    public class MockSubscriber implements Subscriber {

        public Boolean done = false;

        public Function doOn;

        public Integer counter = 0;

        public Integer numberOfIterations = 0;

        public Exception error;

        public void onComplete() {
            this.done=true;
        }

        public void onNext(Object o) {
            doOn.apply(o);
            counter ++;
        }

        public void onError(Exception e) {
            this.error = e;
        }

        public void onSubscribe(Subscription subscription) {
            subscription.request(numberOfIterations);
        }

    }

    private class MockErrorFunction implements Function {
        public Object apply(Object o) {
            throw new InvalidArgumentException('Fail!');
        }
    }

    public class NamePredicate implements Predicate {
        public Boolean apply(Object arg) {
            SObject o = (SObject)arg;
            return o.get('Name') != null && o.get('Name').equals('Foo');
        }
    }

    public class MapToString implements Function {
        public Object apply(Object o) {
            return ((Sobject)o).get('Name');
        }
    }

    public class EvenOnly implements Predicate {
        public Boolean apply(Object o) {
            return Math.mod((Integer)o,2) ==0;
        }
    }

    public class StringValueOf implements Function {
        public Object apply(Object o) {
            return String.valueOf(o);
        }
    }

    public class CountingCollector implements BiCollectorFunction {
        public Object apply(Object one, Object two) {
            Integer count;
            if(one == null) {
                count = 1;
            } else {
                count = Integer.valueOf(one) + 1;
            }
            return count;
        }
    }

    @IsTest
    public static void testListObjectConstructor() {
        List<Object> objects = new List<Integer>{1,2};
        Stream stream = Stream.streamOf(objects);
        Integer count = 0;
        while(stream.hasNext()) {
            stream.next();
            count ++;
        }
        System.assert(count == 2);
    }

    @IsTest
    public static void testSetConstructor() {
        Set<Object> objects = new Set<Object>{new Account(name = 'Test'),new Account(name = 'Test2')};
        Stream stream = Stream.streamOf(objects);
        Integer count = 0;
        while(stream.hasNext()) {
            stream.next();
            count ++;
        }
        System.assert(count == 2);
    }

    @IsTest
    public static void streamsShouldChainSuccessfully() {
        Integer count = (Integer)Stream.streamOf(new List<SObject>{new Account(name='Foo'),
                                                 new Account(name='Foo'),
                                                 new Account(name ='Bar')})
                                                 .filter(new NamePredicate())
                                                 .mapValues(new MapToString())
                                                 .collect(new CountingCollector());
        System.assertEquals(2,count, 'Stream did not process the correct filtered values');
    }

    @IsTest
    public static void streamsShouldCollectToListSuccessfully() {
        List<String> count = (List<String>)Stream.streamOf(new List<SObject>{new Account(name='Foo'),
        new Account(name='Foo'),
        new Account(name ='Bar')})
        .filter(new NamePredicate())
        .mapValues(new MapToString())
        .collect(new List<String>());
        System.assertEquals(2,count.size(), 'Stream did not process the correct filtered values');
    }

    @IsTest
    public static void hugeStreamsShouldNotExplode() {
        List<Integer> bigListOInts = new List<Integer>();
        for(Integer i = 0;i <= 10000; i++) {
            bigListOInts.add(i);
        }

        Integer count = (Integer)Stream.streamOf(bigListOInts)
        .filter(new EvenOnly())
        .mapValues(new StringValueOf())
        .collect(new CountingCollector());

        System.assertEquals(5001,count,'Did not filter the right number of values!');
    }

    @IsTest
    public static void subscriptionShouldRunNumberOfTimesRequestedOnly() {
        MockSubscriber subscriber = new MockSubscriber();
        subscriber.numberOfIterations = 2;
        subscriber.doOn = Functions.identity();
        Stream stream = Stream.streamOf(new List<Object>{1,2,3});
        stream.subscribe(subscriber);

        System.assertEquals(2,subscriber.counter);
        System.assertEquals(true,Stream.hasNext());
        
    }

    @IsTest 
    public static void subscriptionShouldLogExceptionsWhenEncountered() {
        MockSubscriber subscriber = new MockSubScriber() ;
        subscriber.numberOfIterations =1;
        subscriber.doOn = new MockErrorFunction();
        Stream stream = Stream.StreamOf(new List<Object>{1,2,3});
        stream.subscribe(subscriber);
        System.assert(subscriber.error != null);
        System.assertEquals('Fail!',subscriber.error.getMessage());
    }

    @IsTest
    public static void subscriptionShouldCompleteWhenNumberofItemsLessThanRequested() {
        MockSubscriber subscriber = new MockSubscriber();
        subscriber.numberOfIterations = 32767;
        subscriber.doOn = Functions.identity();
        Stream stream = Stream.streamOf(new List<Object>{1,2,3});
        stream.subscribe(subscriber);

        System.assertEquals(3,subscriber.counter);
        System.assertEquals(true, subscriber.done);
        System.assertEquals(false,Stream.hasNext());
        
    }
}
