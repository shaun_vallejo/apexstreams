/*
 * Created on Tue Dec 08 2020
 *
 * Copyright (c) 2020 Sparta Systems
 * Author: Shaun Vallejo
 */

global interface BiCollectorFunction {
   Object apply(Object one, Object two);
}
