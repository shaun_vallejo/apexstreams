/*
 * Created on Tue Dec 08 2020
 *
 * Copyright (c) 2020 Sparta Systems
 * Author: Shaun Vallejo
 */

global interface Function {
    Object apply(Object arg);
}
