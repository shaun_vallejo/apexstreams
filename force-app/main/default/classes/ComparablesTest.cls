/*
 * Created on Tue Dec 08 2020
 *
 * Copyright (c) 2020 Sparta Systems
 * Author: Shaun Vallejo
 */
@IsTest
public with sharing class ComparablesTest {
   @IsTest
   public static void testConstructor() {
       Comparables.toComparable(Double.valueOf('2.5'));
       Comparables.toComparable(1);
       Comparables.toComparable(Long.valueOf('1'));
       Comparables.toComparable(Date.today());

   }

   @IsTest
   public static void comparablesShouldCompareProperly() {
       List<Comparable> comparables = new List<Comparable>{Comparables.toComparable(3),Comparables.toComparable(2),Comparables.toComparable(4)};
       comparables.sort();
       System.assertEquals(2,((Comparables.GenericComparable)comparables.get(0)).getComparable());
       System.assertEquals(3,((Comparables.GenericComparable)comparables.get(1)).getComparable());
       System.assertEquals(4,((Comparables.GenericComparable)comparables.get(2)).getComparable());
   }
}
