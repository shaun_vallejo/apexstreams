/*
 * Created on Tue Dec 08 2020
 *
 * Copyright (c) 2020 Sparta Systems
 * Author: Shaun Vallejo
 */

global inherited sharing class FilteringStream extends Stream {

    final Predicate predicate;

    private Object next;

    public FilteringStream(Iterator<Object> stream,Predicate predicate) {
        super(stream);
        this.predicate = predicate;
    }

    global override Boolean hasNext() {
        if(next != null) {
            return true;
        }

        Boolean foundNext = false;
        while(!foundNext && super.hasNext() ) {//Continue iterating until something matches.
            Object next = super.next();
            if(this.predicate.apply(next)) {
                this.next = next;
                foundNext = true;
            }
        } 
        return foundNext;
    }

    global override Object next() {
        if(this.hasNext() ) {
            Object toReturn = next;
            this.next = null;
            return toReturn;
        } else {
            throw new NoSuchElementException();
        }
    }

}
