@IsTest
public with sharing class MappingStreamTest {

    private class MappingFunction implements Function {
        public Object apply(Object arg) {
            Account acc = (Account)arg;
            return acc.Name;
        }
    }
    @IsTest
    public static void mappingStreamShouldMapObjectsProperly() {
        Account acc = new Account(name='Foo');
        Account acc2 = new Account(name='Bar');
        Account acc3 = new Account(name ='Foo');
        List<Object> accounts = new List<Account>{acc,acc2,acc3};
        MappingStream stream = new MappingStream(accounts.iterator(), new MappingFunction());
        Integer numberOfFoo =0;
        Integer numberOfBar = 0;
        while(stream.hasNext()) {
            String name = (String)stream.next();
            if(name.equals('Foo')) {
                numberOfFoo++;
            } else {
                numberOfBar++;
            }
        }

        System.assertEquals(2,numberOfFoo,'All foos did not populate');
        System.assertEquals(1,numberOfBar,'All Bar did not populate');
    }
}
