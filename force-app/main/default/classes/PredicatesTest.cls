/*
 * Created on Tue Dec 08 2020
 *
 * Copyright (c) 2020 Sparta Systems
 * Author: Shaun Vallejo
 */

@IsTest
public with sharing class PredicatesTest {
  @IsTest
  public static void eqShouldReturnProperly() {
      System.assert(Predicates.eq(1).apply(1));
      System.assert(Predicates.eq('1').apply('1'));
      Account account = new Account();
      System.assert(Predicates.eq(account).apply(account));
  }

  @IsTest
  public static void eqShouldReturnProperlyIfFalse() {
    System.assert(!Predicates.eq(1).apply(2));
    System.assert(!Predicates.eq('1').apply('3'));
    Account account = new Account();
    System.assert(!Predicates.eq(account).apply(new Account(name='Cookie')));
  }

  @IsTest
  public static void gtShouldReturnGreaterThan() {
      Predicate pred = Predicates.gt(Comparables.toComparable(1));
        System.assert(pred.apply(2));

       pred = Predicates.gt(Comparables.toComparable(Date.today()));
       Date newDate = Date.today().addDays(2);
       System.assert(pred.apply(newDate));
  }

  @IsTest
  public static void gtShouldReturnFalseWhenLessThanOrEqualTo() {
    Predicate pred = Predicates.gt(Comparables.toComparable(1));
    System.assert(!pred.apply(0));
    System.assert(!pred.apply(1));
  }

  @IsTest
  public static void ltShouldReturnTrueWhenLessThan() {
      Predicate pred = Predicates.lt(Comparables.toComparable(1));
      System.assert(pred.apply(0));
      System.assert(pred.apply(-1));
  }

  @IsTest
  public static void ltShouldReturnFalseWhenGreaterThanOrEqual() {
    Predicate pred = Predicates.lt(Comparables.toComparable(1));
    System.assert(!pred.apply(1));
    System.assert(!pred.apply(2));
  }

  @IsTest
  public static void conjunctiveShouldReturnTrueWhenBothPredicatesAreTrue() {
      Predicate pred = Predicates.gt(Comparables.toComparable(1))
      .andAlso(Predicates.gt(Comparables.toComparable(2)));

      System.assert(pred.apply(3));
  }

  @IsTest
  public static void conjunctiveShouldReturnFalseWhenOneOrMorePredicatesAreFalse() {
    Predicate pred = Predicates.gt(Comparables.toComparable(1))
    .andAlso(Predicates.gt(Comparables.toComparable(3)));
    System.assert(!pred.apply(2));
  }

  @IsTest
  public static void disjunctiveShouldReturnTrueWhenOneConditionOrBothAreTrue() {
    Predicate pred = Predicates.gt(Comparables.toComparable(1))
    .orIs(Predicates.gt(Comparables.toComparable(3)));

    System.assert(pred.apply(4));
    System.assert(pred.apply(2));
    
  }

  @IsTest
  public static void disjunctiveShouldReturnTrueBothConditionsAreFalse() {
    Predicate pred = Predicates.gt(Comparables.toComparable(1))
    .orIs(Predicates.gt(Comparables.toComparable(3)));

    System.assert(!pred.apply(0));
    System.assert(!pred.apply(-1));
  }

  @IsTest
  public static void negationShouldReturnTheOppositeOfResult() {
    Predicate pred = Predicates.isnt(Predicates.gt(Comparables.toComparable(1))
    .orIs(Predicates.gt(Comparables.toComparable(3))));

    System.assert(!pred.apply(4));
    System.assert(!pred.apply(2));
  }

  @IsTest
  public static void memberOfShouldReturnTrueWhenObjectIsInSet() {
    Set<Object> validValues = new Set<Object>{'One','Two','Three'};
    System.assertEquals(true, Predicates.memberOf(validValues).apply('One'));
  }

  @IsTest
  public static void memberOfShouldReturnFalseWhenObjectNotInSet() {
    Set<Object> validValues = new Set<Object>{'One','Two','Three'};
    System.assertEquals(false, Predicates.memberOf(validValues).apply('Four'));
  }

  @IsTest
  public static void pipingShouldReturnValidTrue() {
    Account account = new Account(name='Foo');
    System.assertEquals(true, Predicates.piping(Functions.fieldValueOf('Name'), Predicates.eq('Foo')).apply(account));
  }

  @IsTest
  public static void pipingShouldReturnFalseIfFalse() {
    Account account = new Account(name='Foo');
    System.assertEquals(false, Predicates.piping(Functions.fieldValueOf('Name'), Predicates.eq('Bar')).apply(account));
  }
}
