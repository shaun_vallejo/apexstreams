/*
 * Created on Tue Dec 08 2020
 *
 * Copyright (c) 2020 Sparta Systems
 * Author: Shaun Vallejo
 */

public with sharing class Comparables {
    public class GenericComparable implements Comparable {
        public final Comparator comparator;
        public final Object base;
        public GenericComparable(Comparator comparator,Object base) {
            this.comparator = comparator;
            this.base = base;
        }

        public Integer compareTo(Object o) {
            if(o instanceof GenericComparable) {
                return comparator.compare(base, ((GenericComparable)o).getComparable());
            } else  {
                return comparator.compare(base, o);
            } 
            
        }
        
        public Object getComparable() {
            return this.base;
        }
    }

    public static Comparable toComparable(Double base) {
        return new GenericComparable(Comparators.doubles(Comparators.Direction.ASCENDING)
        ,base);
    }

    public static Comparable toComparable(Integer base) {
        return new GenericComparable(Comparators.int(Comparators.Direction.ASCENDING)
        ,base);
    }

    public static Comparable toComparable(Date base) {
        return new GenericComparable(Comparators.dates(Comparators.Direction.ASCENDING)
        ,base);
    }

    public static Comparable toComparable(Long base) {
        return new GenericComparable(Comparators.longs(Comparators.Direction.ASCENDING)
        ,base);
    }

    public static Comparable toComparable(Comparator comparator, Object base) {
        return new GenericComparable(comparator,base);
    }

}
