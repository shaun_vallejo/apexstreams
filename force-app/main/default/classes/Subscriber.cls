global interface Subscriber {
    void onComplete();

    void onError(Exception e);

    void onNext(Object o);

    void onSubscribe(Subscription subscription);
}
