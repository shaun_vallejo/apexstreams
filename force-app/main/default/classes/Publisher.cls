public interface Publisher {
    void subscribe(Subscriber subscriber);
}
