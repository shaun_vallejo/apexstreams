global virtual class Stream implements Iterator<Object> ,Publisher , Subscription{

    private final Iterator<Object> stream;

    private Subscriber subscriber;

    private Boolean canceled = false;

    public static Stream streamOf(List<Object> objects) {
        return new Stream(objects.iterator());
    }

    public static Stream streamOf(Set<Object> objects) {
        return new Stream(objects.iterator());
    }

    public static Stream streamOf(List<SObject> objects) {
        List<Object> sillyCast = (List<Object>)objects;
        return new Stream(sillyCast.iterator());
    }

    global Stream(Iterator<Object> stream) {
        this.stream = stream;
    }

    global virtual Boolean hasNext() {
        return this.stream.hasNext();
    }

    global virtual Object next() {
        return this.stream.next();
    }

    global List<Object> collect(List<Object> collection) {
        
        while(hasNext()) {
            collection.add(next());
        }
        return collection;
    }

    global Object collect(BiCollectorFunction collectionFunction) {
        Object collectorResult;
        while(hasNext()) {
            collectorResult = collectionFunction.apply(collectorResult, next());
        }
        return collectorResult;
    }

    global Stream filter(Predicate filteringFunction) {
        return new FilteringStream(this,filteringFunction);
    }

    global Stream mapValues(Function mappingFunction) {
        return new MappingStream(this,mappingFunction);
    }

    global void forEach(Function exectuable) {
        while(hasNext()) {
            exectuable.apply(next());
        }
    }

    global void subscribe(Subscriber subscriber) {
        this.subscriber = subscriber;
        subscriber.onSubscribe(this);
        
    }

    global void request(Long numberOfItems) {
        Long count = 0;
        while(hasNext() && count < numberOfItems && !canceled) {
            try {
                this.subscriber.onNext(next());
                count++;
            } catch(Exception e) {
                this.subscriber.onError(e);
            }
        } 
        if(!this.hasNext()) { //Nothing else to do
            subscriber.onComplete();
        } 
        this.canceled = false;
    }

    global void cancel() {
        this.canceled = canceled;
    }

}
