/*
 * Created on Tue Dec 08 2020
 *
 * Copyright (c) 2020 Sparta Systems
 * Author: Shaun Vallejo
 */
@IsTest
public with sharing class ComparatorsTest {

    @IsTest
    public static void testGreaterThanResult() {
        assertGT(Comparators.dates(Comparators.Direction.ASCENDING),Date.today(), Date.today() -1);
        assertGT(Comparators.longs(Comparators.Direction.ASCENDING),2,1);
        assertGT(Comparators.int(Comparators.Direction.ASCENDING), 2, 1);
        assertGT(Comparators.doubles(Comparators.Direction.ASCENDING), 2.5, 2.4);

        assertGT(Comparators.dates(Comparators.Direction.DESCENDING),Date.today(),Date.today() + 1);
        assertGT(Comparators.longs(Comparators.Direction.DESCENDING),1,2);
        assertGT(Comparators.int(Comparators.Direction.DESCENDING), 1,2);
        assertGT(Comparators.doubles(Comparators.Direction.DESCENDING), 2.4, 2.5);
    }

    @IsTest
    public static void testLessThanResult() {
        assertLT(Comparators.dates(Comparators.Direction.DESCENDING),Date.today(), Date.today() -1);
        assertLT(Comparators.longs(Comparators.Direction.DESCENDING),2,1);
        assertLT(Comparators.int(Comparators.Direction.DESCENDING), 2, 1);
        assertLT(Comparators.doubles(Comparators.Direction.DESCENDING), 2.5, 2.4);

        assertLT(Comparators.dates(Comparators.Direction.ASCENDING),Date.today(),Date.today() + 1);
        assertLT(Comparators.longs(Comparators.Direction.ASCENDING),1,2);
        assertLT(Comparators.int(Comparators.Direction.ASCENDING), 1,2);
        assertLT(Comparators.doubles(Comparators.Direction.ASCENDING), 2.4, 2.5);

    }

    @IsTest
    public static void testEqResult() {
        assertEQ(Comparators.dates(Comparators.Direction.DESCENDING),Date.today(), Date.today());
        assertEQ(Comparators.longs(Comparators.Direction.DESCENDING),1,1);
        assertEQ(Comparators.int(Comparators.Direction.DESCENDING), 1, 1);
        assertEQ(Comparators.doubles(Comparators.Direction.DESCENDING), 2.5, 2.5);

        assertEQ(Comparators.dates(Comparators.Direction.ASCENDING),Date.today(),Date.today());
        assertEQ(Comparators.longs(Comparators.Direction.ASCENDING),1,1);
        assertEQ(Comparators.int(Comparators.Direction.ASCENDING), 1,1);
        assertEQ(Comparators.doubles(Comparators.Direction.ASCENDING), 2.5, 2.5);
    }

    
    private static void assertGT(Comparator comparator,Object one, Object two) {
        System.assertEquals(1, comparator.compare(one, two),'Did not get the appropriate Greater than result for ' + String.valueOf(one) + ' and ' + String.valueOf(two));
    }

    private static void assertLT(Comparator comparator,Object one, Object two) {   
        System.assertEquals(-1, comparator.compare(one, two),'Did not get the appropriate Less than result for ' + String.valueOf(one) + ' and ' + String.valueOf(two));

    }

    private static void assertEQ(Comparator comparator, Object one, Object two) {
        System.assertEquals(0, comparator.compare(one, two),'Did not get the appropriate Equals result for ' + String.valueOf(one) + ' and ' + String.valueOf(two));
    }
}
