/*
 * Created on Tue Dec 08 2020
 *
 * Copyright (c) 2020 Sparta Systems
 * Author: Shaun Vallejo
 */

public with sharing class Comparators {
    public enum Direction{ASCENDING,DESCENDING}

    public class IntegerComparator implements Comparator {
        private final Direction direction;

        public IntegerComparator(Direction direction) {
            this.direction = direction;
        }
        public Integer compare(Object one, Object two) {
            Integer intOne = (Integer)one;
            Integer intTwo = (Integer)two;
            
            if(intOne == intTwo ) {
                return 0;
            } else {
                return mod(intOne > intTwo ? 1 :-1,this.direction) ;
            }
        }
    }

    public class DoubleComparator implements Comparator {
        private final Direction direction;

        public DoubleComparator(Direction direction) {
            this.direction = direction;
        }
        public Integer compare(Object one, Object two) {
            Double intOne = (Double)one;
            Double intTwo = (Double)two;
            
            if(intOne == intTwo ) {
                return 0;
            } else {
                return mod( (intOne > intTwo ? 1 :-1 ), this.direction);
            }
        }
    }

    public class LongComparator implements Comparator {
        private final Direction direction;

        public LongComparator(Direction direction) {
            this.direction = direction;
        }
        public Integer compare(Object one, Object two) {
            Long intOne = (Long)one;
            Long intTwo = (Long)two;
            
            if(intOne == intTwo ) {
                return 0;
            } else {
                return mod( (intOne > intTwo ? 1 :-1 ), this.direction);
            }
        }
    }

    public class DateComparator implements Comparator {
        private final Direction direction;

        public DateComparator(Direction direction) {
            this.direction = direction;
        }
        public Integer compare(Object one, Object two) {
            Date intOne = (Date)one;
            Date intTwo = (Date)two;
            
            if(intOne == intTwo ) {
                return 0;
            } else {
                return mod( (intOne > intTwo ? 1 :-1 ), this.direction);
            }
        }
    }

    private static Integer mod(Integer base, Direction direction) {
        return base * ( (direction == Comparators.Direction.DESCENDING) ? -1 : 1 );
    }

    public static Comparator dates(Direction direction) {
        return new DateComparator(direction);
    }

    public static Comparator int(Direction direction) {
        return new IntegerComparator(direction);
    }

    public static Comparator doubles(Direction direction) {
        return new DoubleComparator(direction);
    }

    public static Comparator longs(Direction direction) {
        return new LongComparator(direction);
    }

}
