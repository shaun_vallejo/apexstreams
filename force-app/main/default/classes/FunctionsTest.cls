/*
 * Created on Tue Dec 08 2020
 *
 * Copyright (c) 2020 Sparta Systems
 * Author: Shaun Vallejo
 */

@IsTest
public with sharing class FunctionsTest {

    @IsTest
    public static void identityShouldReturnItself() {
        Account account = new Account(name='Foo');
        System.assertEquals(account, Functions.identity().apply(account));
    }

    @IsTest
    public static void fieldValueOfShouldGrabFieldValue() {
        Account account = new Account(name = 'Foo');
        System.assertEquals('Foo',Functions.fieldValueOf('Name').apply(account));
    }

    @IsTest
    public static void groupingShouldGroupProperly() {
        List<Account> accs = new List<Account>{
            new Account(name='Foo'),
            new Account(name='Bar'),
            new Account(name='Hello'),
            new Account(name='Foo'),
            new Account(name='Bar')
        };

        Map<Object,List<Object>> groupedByName = 
            (Map<Object,List<Object>>)Stream.streamOf(accs)
                .collect(Functions.grouping(Functions.fieldValueOf('Name')));
        
        System.assertEquals(2,groupedByName.get('Foo').size());
        System.assertEquals(2,groupedByName.get('Bar').size());
        System.assertEquals(1,groupedByName.get('Hello').size());
    }

    @IsTest
    public static void functionChainingShouldApplyAllFunctions() {
        Account account = new Account(name='Foo',Phone='1234567890');
        Object result = Functions.identity()
        .andThen(Functions.FieldValueOf('Phone')).apply(Account);

        System.assertEquals('1234567890', result);
    }

    @IsTest
    public static void functionRoutingShouldRouteWhenPredicateIsPositive() {
        Account account = new Account(name='Foo', Phone='1234567890');
        Object result = Functions.FieldValueOf('Name')
        .orApplyWhen(Functions.fieldValueOf('Phone'),Predicates.piping(Functions.FieldValueOf('Name'),Predicates.eq('Foo')))
        .apply(account);
        System.assertEquals('1234567890', result);
    }

    @IsTest
    public static void functionRoutingShouldNotRouteWhenPredicateIsNegative() {
        Account account = new Account(name='Bar', Phone='1234567890');
        Object result = Functions.FieldValueOf('Name')
        .orApplyWhen(Functions.fieldValueOf('Phone'),Predicates.piping(Functions.FieldValueOf('Name'),Predicates.eq('Foo')))
        .apply(account);
        System.assertEquals('Bar', result);
    }
}
